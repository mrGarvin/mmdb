﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDB.MovieDatabase.Helper
{
    public class TryGetFilePath
    {
        private static string _AppDomainPath = AppDomain.CurrentDomain.BaseDirectory;
        private static string _AssemblyPath = new Uri(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath;
        private static string _DirectoryPath = System.IO.Directory.GetCurrentDirectory();

        public static bool InProjectDirectory(string fileName, out string path)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                path = null;
                return false;
            }

            path = _AppDomainPath;

            for (int i = 0; i < ((path == _DirectoryPath) ? 2 : 3); i++)
            {
                path = System.IO.Path.GetDirectoryName(path);
            }

            path = System.IO.Path.Combine(path, fileName);

            return true;
        }

        public static bool InProjectDirectory(string fileName, string subDirectory, out string path)
        {
            if (string.IsNullOrWhiteSpace(subDirectory))
            {
                path = null;
                return false;
            }

            if (!InProjectDirectory(System.IO.Path.Combine(subDirectory, fileName), out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        public static bool InProjectDirectory(string fileName, string[] subDirectories, out string path)
        {
            if (subDirectories == null || subDirectories.Length == 0)
            {
                path = null;
                return false;
            }
            foreach (string subDirectory in subDirectories)
            {
                if (string.IsNullOrWhiteSpace(subDirectory))
                {
                    path = null;
                    return false;
                }
            }

            if (!InProjectDirectory(System.IO.Path.Combine(System.IO.Path.Combine(subDirectories), fileName), out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        public static bool InSolutionDirectory(string fileName, out string path)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                path = null;
                return false;
            }

            path = _AppDomainPath;

            for (int i = 0; i < ((path == _DirectoryPath) ? 3 : 4); i++)
            {
                path = System.IO.Path.GetDirectoryName(path);
            }

            path = System.IO.Path.Combine(path, fileName);

            return true;
        }

        public static bool InSolutionDirectory(string fileName, string subDirectory, out string path)
        {
            if (string.IsNullOrWhiteSpace(subDirectory))
            {
                path = null;
                return false;
            }

            if (!InSolutionDirectory(System.IO.Path.Combine(subDirectory, fileName), out path))
            {
                path = null;
                return false;
            }

            return true;
        }

        public static bool InSolutionDirectory(string fileName, string[] subDirectories, out string path)
        {
            if (subDirectories == null || subDirectories.Length == 0)
            {
                path = null;
                return false;
            }
            foreach (string subDirectory in subDirectories)
            {
                if (string.IsNullOrWhiteSpace(subDirectory))
                {
                    path = null;
                    return false;
                }
            }

            if (!InSolutionDirectory(System.IO.Path.Combine(System.IO.Path.Combine(subDirectories), fileName), out path))
            {
                path = null;
                return false;
            }

            return true;
        }
    }
}
