﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDB.MovieDatabase.Helper
{
    public static class StringUtilities
    {
        public static bool Contains(this string str, string value, bool ignoreCase)
        {
            if (ignoreCase)
            {
                str = str.ToLower();
                value = value.ToLower();
            }

            return str.Contains(value);
        }
    }
}
