﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MMDB.MovieDatabase.Repositories
{
    public class CastOrCrewRepository
    {
        private readonly string fileName = $"{nameof(CastOrCrew)}.xml";

        private List<CastOrCrew> people;

        public int NumberOfActors
        {
            get
            {
                CountCastOrCrew();
                return _numberOfActors;
            }
            private set
            {
                _numberOfActors = value;
            }
        }
        private int _numberOfActors;

        public int NumberOfDirectors
        {
            get
            {
                CountCastOrCrew();
                return _numberOfDirectors;
            }
            private set
            {
                _numberOfDirectors = value;
            }
        }
        private int _numberOfDirectors;

        public int NumberOfMultitaskers
        {
            get
            {
                CountCastOrCrew();
                return _numberOfMultitaskers;
            }
            set
            {
                _numberOfMultitaskers = value;
            }
        }
        private int _numberOfMultitaskers;

        public int NumberOfUnemployees
        {
            get
            {
                CountCastOrCrew();
                return _numberOfUnemployed;
            }
            set
            {
                _numberOfUnemployed = value;
            }
        }
        private int _numberOfUnemployed;

        private static CastOrCrewRepository _instance;

        public static CastOrCrewRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CastOrCrewRepository();
                }
                return _instance;
            }
        }

        private CastOrCrewRepository()
        {
            people = new List<CastOrCrew>();
            Load();
        }

        public void Add(CastOrCrew castOrCrew)
        {
            people.Add(castOrCrew);
        }

        private void CountCastOrCrew()
        {
            int actors = 0, directors = 0, multitaskers = 0, unemployed = 0;

            foreach (CastOrCrew person in people)
            {
                if (person.IsMultitasker)
                {
                    multitaskers++;
                    actors++;
                    directors++;
                }
                else if (person.IsActor)
                {
                    actors++;
                }
                else if (person.IsDirector)
                {
                    directors++;
                }
                else if (person.IsUnemployed)
                {
                    unemployed++;
                }
            }

            NumberOfMultitaskers = multitaskers;
            NumberOfActors = actors;
            NumberOfDirectors = directors;
            NumberOfUnemployees = unemployed;
        }
 
        public CastOrCrew FindBy(string name)
        {
            return people.Find(x => x.Name.ToLower() == name.ToLower());
        }

        public CastOrCrew FindBy(Guid id)
        {
            return people.Find(x => x.Id == id);
        }

        public IEnumerable<CastOrCrew> Search(string searchText)
        {
            return people.Where(p => p.DateOfBirth.ToString().Contains(searchText, true)
                                    || p.Name.Contains(searchText, true));
        }

        public IEnumerable<CastOrCrew> AllPeople()
        {
            return people;
        }

        public void Load()
        {
            try
            {
                string castOrCrewFile;
                
                if (TryGetFilePath.InProjectDirectory(fileName, out castOrCrewFile))
                {
                    using (Stream stream = File.Open(castOrCrewFile, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<CastOrCrew>));
                        people = (List<CastOrCrew>)serializer.Deserialize(stream);
                    }
                }
                else
                {
                    throw new Exception($"The file path is not correct or the file '{castOrCrewFile}' does not exist.");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal void Save()
        {
            string castOrCrewFile;

            try
            {
                if (TryGetFilePath.InProjectDirectory(fileName, out castOrCrewFile))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<CastOrCrew>));
                    using (Stream stream = File.Open(castOrCrewFile, FileMode.Create))
                    {
                        serializer.Serialize(stream, people);
                    }
                }
                else
                {
                    throw new Exception($"The file path is not correct or the file '{castOrCrewFile}' does not exist.");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
