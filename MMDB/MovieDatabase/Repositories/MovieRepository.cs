﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Domain.ValueObjects;
using MMDB.MovieDatabase.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MMDB.MovieDatabase.Repositories
{
    public class MovieRepository
    {
        private readonly string fileName = $"{nameof(Movie)}s.xml";

        private List<Movie> movies;

        private static MovieRepository _instance;

        public MovieRepository()
        {
            movies = new List<Movie>();
            Load();
        }
        public static MovieRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MovieRepository();
                }
                return _instance;
            }
        }

        internal IEnumerable<Movie> GetAllMovies()
        {
            return movies;
        }

        public void AddMovie(Movie movie)
        {
            movies.Add(movie);
        }

        public Movie FindBy(string title)
        {
            return movies.Find(x => x.Title.ToLower() == title.ToLower());
        }

        public Movie FindBy(Guid id)
        {
            return movies.Find(x => x.Id == id);
        }

        public IEnumerable<Movie> Search(string searchText)
        {
            return movies.Where(m => m.Title.Contains(searchText, true) || m.ProductionYear.ToString().Contains(searchText, true));
        }

        public void Load()
        {
            CastOrCrewRepository castOrCrewRepository = CastOrCrewRepository.Instance;

            string moviesFile;

            try
            {
                if (TryGetFilePath.InProjectDirectory(fileName, out moviesFile) && File.Exists(moviesFile))
                {
                    using (Stream stream = File.Open(moviesFile, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<Movie>));
                        movies = (List<Movie>)serializer.Deserialize(stream);
                    }
                }
                else
                {
                    throw new Exception($"The file path is not correct or the file '{moviesFile}' does not exist.");
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        internal void Save()
        {
            string moviesFile;

            try
            {
                if (TryGetFilePath.InProjectDirectory(fileName, out moviesFile) && File.Exists(moviesFile))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Movie>));
                    using (Stream stream = File.Open(moviesFile, FileMode.Create))
                    {
                        serializer.Serialize(stream, movies);
                    }
                }
                else
                {
                    throw new Exception($"The file path is not correct or the file '{moviesFile}' does not exist.");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
