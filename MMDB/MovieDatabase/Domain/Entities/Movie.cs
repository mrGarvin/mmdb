﻿using MMDB.MovieDatabase.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMDB.MovieDatabase.Domain
{
    public class Movie
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public ProductionYear ProductionYear { get; set; }

        public HashSet<Guid> ActorIds { get; set; }

        public HashSet<Guid> DirectorIds { get; set; }

        public Movie()
        {
            ActorIds = new HashSet<Guid>();
            DirectorIds = new HashSet<Guid>();
            Id = Guid.NewGuid();
        }
        public Movie(string title, ProductionYear productionYear) : this()
        {
            
            ProductionYear = productionYear;
            Title = title;
        }

        public void AddActor(CastOrCrew actor)
        {
            if (actor==null)
            {
                return;
            }
            actor.ActedMovieIds.Add(this.Id);
            ActorIds.Add(actor.Id);
        }

        public void AddDirector(CastOrCrew director)
        {
            if (director == null)
            {
                return;
            }
            director.ActedMovieIds.Add(this.Id);
            DirectorIds.Add(director.Id);
        }

        public string ToString(bool ignoreGuid = false, bool ignoreProductionYear = false)
        {
            StringBuilder toStringBuilder = new StringBuilder();

            if (ignoreGuid == false)
            {
                toStringBuilder.Append(Id.ToString());
                toStringBuilder.Append(", ");
            }

            toStringBuilder.Append(Title);

            if (ignoreProductionYear == false)
            {
                toStringBuilder.Append(", (");
                toStringBuilder.Append(ProductionYear.ToString());
                toStringBuilder.Append(")");
            }
            
            return toStringBuilder.ToString();
        }
    }
}
