﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMDB.MovieDatabase.Domain
{
    public class CastOrCrew
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool IsDirector { get { return DirectedMovieIds.Count > 0; }  }

        public bool IsActor { get { return ActedMovieIds.Count > 0; } }

        public bool IsMultitasker { get { return (IsActor && IsDirector); } }

        public bool IsUnemployed { get { return (!IsActor && !IsDirector); } }

        public HashSet<Guid> ActedMovieIds { get; set; }

        public HashSet<Guid> DirectedMovieIds { get; set; }
        public string JobTitle
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                bool needsComma = false;
                if (IsActor)
                {
                    builder.Append("Actor");
                    needsComma = true;
                }
                if (IsDirector)
                {
                    if (needsComma)
                    {
                        builder.Append(", ");
                    }
                    builder.Append("Director");
                    needsComma = true;
                }
                return builder.ToString();
            }
        }

        public CastOrCrew()
        {
            ActedMovieIds = new HashSet<Guid>();
            DirectedMovieIds = new HashSet<Guid>();
            Id = Guid.NewGuid();
        }

        public CastOrCrew(string name, DateTime dateOfBirth):this()
        {
            Name = name;
            DateOfBirth = dateOfBirth;
        }

        public string ToString(bool ignoreGuid = false, bool ignoreDateOfBirth = false, bool ignoreJobTitle = false)
        {
            StringBuilder toStringBuilder = new StringBuilder();

            if (ignoreGuid == false)
            {
                toStringBuilder.Append(Id.ToString());
                toStringBuilder.Append(", ");
            }

            toStringBuilder.Append(Name);

            if (ignoreDateOfBirth == false)
            {
                toStringBuilder.Append(", ");
                toStringBuilder.Append(DateOfBirth.ToShortDateString());
            }
            if (ignoreJobTitle == false)
            {
                toStringBuilder.Append(", ");
                toStringBuilder.Append(JobTitle);
            }

            return toStringBuilder.ToString();
        }
    }
}
