﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Repositories;
using System;
using System.Collections.Generic;

namespace MMDB.MovieDatabase.Services
{
    public class MovieService
    {
        private MovieRepository repository;

        public MovieService()
        {
            repository = MovieRepository.Instance;
        }

        public void AddMovie(Movie movie)
        {
            repository.AddMovie(movie);
        }

        public Movie FindBy(string title)
        {
            return repository.FindBy(title);
        }

        public Movie FindBy(Guid id)
        {
            return repository.FindBy(id);
        }

        public IEnumerable<Movie> Search(string searchText)
        {
            return repository.Search(searchText);
        }

        public IEnumerable<Movie> GetAllMovies() => repository.GetAllMovies();

        internal void Save()
        {
            repository.Save();
        }
    }
}
