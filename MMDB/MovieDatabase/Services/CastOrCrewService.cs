﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Repositories;
using System;
using System.Collections.Generic;

namespace MMDB.MovieDatabase.Services
{
    public class CastOrCrewService
    {
        private CastOrCrewRepository repository;

        public CastOrCrewService()
        {
            repository = CastOrCrewRepository.Instance;
        }

        public void Add(CastOrCrew castOrCrew)
        {
            repository.Add(castOrCrew);
        }

        public CastOrCrew FindBy(Guid id)
        {
            return repository.FindBy(id);
        }

        public CastOrCrew FindBy(string name)
        {
            return repository.FindBy(name);
        }

        public IEnumerable<CastOrCrew> Search(string searchText)
        {
            return repository.Search(searchText);
        }

        public IEnumerable<CastOrCrew> GetAllPeople() => repository.AllPeople();

        public int NumberOfActors() => repository.NumberOfActors;

        public int NumberOfDirectors() => repository.NumberOfDirectors;

        public int NumberOfMultitaskers() => repository.NumberOfMultitaskers;

        public int NumberOfUnemployees() => repository.NumberOfUnemployees;

        internal void Save()
        {
            repository.Save();
        }
    }
}
