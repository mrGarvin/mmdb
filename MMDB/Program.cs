﻿using System;
using System.Collections.Generic;
using MMDB.MovieDatabase.Services;
using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Domain.ValueObjects;
using System.Linq;

namespace MMDB
{
    class Program
    {
        private MovieService movieService;
        private CastOrCrewService castOrCrewService;
        private bool stopRuning;
        private bool hideGuids;
        private Dictionary<char, Action> commandMapper;
        private Dictionary<char, Action> hiddenCommandMapper;

        static void Main(string[] args)
        {
            new Program().MainLoop();
        }

        public Program()
        {
            movieService = new MovieService();
            castOrCrewService = new CastOrCrewService();
            stopRuning = false;
            hideGuids = false;
            commandMapper = new Dictionary<char, Action>();
            commandMapper.Add('1', NewMovie);
            commandMapper.Add('2', FindMovie);
            commandMapper.Add('3', FindActorOrDirector);
            commandMapper.Add('4', Search);
            commandMapper.Add('q', () => stopRuning = true);
            commandMapper.Add('p', SubMenu);

            hiddenCommandMapper = new Dictionary<char, Action>()
            {
                { 'm', PrintAllMovies },
                { 'M', PrintAllMoviesDetailed },
                { 'c', PrintAllCastOrCrew },
                { 'C', PrintAllCastOrCrewDetailed }
            };
        }

        private void MainLoop()
        {
            if (AskForConformationTo("Do you want to hide Guid's when printing movies and cast/crew?", true))
            {
                hideGuids = true;
            }
            do
            {
                PrintMenu();
                var command = GetCommand(false);
                Console.WriteLine();
                ExecuteCommand(command);

            } while (!stopRuning);
        }

        private void PrintMenu()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("+-----------------------------------------------------------------------------+");
            Console.WriteLine("|   1='New movie', 2='Find movie', 3='Find cast/crew', 4='Search', q='Quit'   |");
            Console.WriteLine("+-----------------------------------------------------------------------------+");
            Console.WriteLine();
        }

        private void PrintSubMenu()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("+---------------------------------+");
            Console.WriteLine("|            Print all:           |");
            Console.WriteLine("|   m='movies',    M='detailed'   |");
            Console.WriteLine("|   c='cast/crew', C='detailed'   |");
            Console.WriteLine("+---------------------------------+");
            Console.WriteLine();
        }

        private void SubMenu()
        {
            do
            {
                PrintSubMenu();
                try
                {
                    Action method = hiddenCommandMapper[GetCommand(true)];
                    Console.WriteLine();
                    method();
                }
                catch
                {
                    Console.WriteLine("Unknown command");
                }
                Console.WriteLine();
            } while (AskForConformationTo("Do you want to stay in this sub-menu?", true));
        }

        private char GetCommand(bool hideKey)
        {
            return Console.ReadKey(hideKey).KeyChar;
        }

        private void ExecuteCommand(char command)
        {
            try
            {
                var method = commandMapper[command];
                method();
            }
            catch
            {
                Console.WriteLine("Unknown command");
            }
        }


        private void FindActorOrDirector()
        {
            PrintFindActorDirectorHeader();
            string name;
            AskFor(out name, "Enter the name of the actor or direcotr you're searching for: ");
            var castOrCrew = castOrCrewService.FindBy(name);
            Console.Clear();
            PrintFindActorDirectorHeader();
            if (castOrCrew == null)
            {
                Console.WriteLine($"Cannot find {name}");
            }
            else
            {
                PrintCastOrCrew(castOrCrew);
            }

            Pause(true);
        }

        private void Pause(bool hideKey = false)
        {
            Console.WriteLine();
            Console.Write("Press any key to return to main menu...");
            Console.ReadKey(hideKey);
        }

        private void PrintNewMovieHeader()
        {
            Console.Clear();
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine("|                       Add a new movie                           |");
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine();
        }

        private void PrintFindActorDirectorHeader()
        {
            Console.Clear();
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine("|             Search for an actor or director                     |");
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine();
        }

        private void PrintFindMovieHeader()
        {
            Console.Clear();
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine("|                       Search for a movie                        |");
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine();
        }

        private void PrintSearchHeader()
        {
            Console.Clear();
            Console.Clear();
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine("|                              Search                             |");
            Console.WriteLine("+-----------------------------------------------------------------+");
            Console.WriteLine();
        }

        private void FindMovie()
        {
            PrintFindMovieHeader();

            string title;
            AskFor(out title, "Enter the title of the movie you are searching for: ");

            var movie = movieService.FindBy(title);
            Console.Clear();
            PrintFindMovieHeader();
            if (movie == null)
            {
                Console.WriteLine($"Cannot find a movie called {title}");
            }
            else
            {
                PrintMovie(movie);
            }

            Pause(true);
        }

        private void Search()
        {
            PrintSearchHeader();

            string searchText;
            AskFor(out searchText, "Enter search text: ");

            List<Movie> movieList = movieService.Search(searchText).ToList();
            
            foreach (Movie movie in movieList)
            {
                Console.WriteLine(movie.ToString(hideGuids));
            }

            List<CastOrCrew> castOrCrewList = castOrCrewService.Search(searchText).ToList();
            
            foreach (CastOrCrew castOrCrew in castOrCrewList)
            {
                Console.WriteLine(castOrCrew.ToString(hideGuids));
            }

            if (movieList == null || movieList.Count == 0)
            {
                Console.WriteLine("No movies found.");
            }
            if (castOrCrewList == null || castOrCrewList.Count == 0)
            {
                Console.WriteLine("No actors or directors found.");
            }

            Pause(true);
        }

        private void NewMovie()
        {
            PrintNewMovieHeader();

            string title;
            AskFor(out title, "Enter the title of the movie: ");

            ProductionYear productionYear;
            AskFor(out productionYear, "Enter the year the movie was produced: ");

            Movie movie = new Movie(title, productionYear);
            movieService.AddMovie(movie);

            string name;
            
            do
            {
                AskFor(out name, "Enter the name of the director: ");
                CastOrCrew director = castOrCrewService.FindBy(name);
                if (director == null)
                {
                    director = NewCastOrCrew(name);
                }
                castOrCrewService.Add(director);
                movie.AddDirector(director);
            } while (AskForConformationTo("Do you want to add a new director to the movie?", true));
            
            do
            {
                AskFor(out name, "Enter the name of an actor: ");
                CastOrCrew actor = castOrCrewService.FindBy(name);
                if (actor == null)
                {
                    actor = NewCastOrCrew(name);
                }
                castOrCrewService.Add(actor);
                movie.AddActor(actor);
            } while (AskForConformationTo("Do you want to add a new actor to the movie?", true));
            Save();
        }

        private void Save()
        {
            try
            {
                castOrCrewService.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to store CastOrCrew");
                Console.WriteLine(e);
            }
            try
            {
                movieService.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to store Movies");
                Console.WriteLine(e.ToString());
            }
        }

        private CastOrCrew NewCastOrCrew(string name)
        {
            DateTime dateOfBirth;
            AskFor(out dateOfBirth, "Enter the date of birth: ");
            return new CastOrCrew(name, dateOfBirth);
        }

        private void PrintMovie(Movie movie)
        {
            Console.WriteLine(movie.ToString(hideGuids));
            if (movie.DirectorIds.Count > 1)
            {
                Console.WriteLine("List of directors:");
                Console.WriteLine("------------------");
            }
            else
            {
                Console.Write("Director: ");
            }
            foreach (var directorId in movie.DirectorIds)
            {
                Console.WriteLine($"{castOrCrewService.FindBy(directorId).ToString(true, false, true)}");
            }
            Console.WriteLine("List of actors:");
            Console.WriteLine("---------------");
            foreach (var actorId in movie.ActorIds)
            {
                Console.WriteLine($"{castOrCrewService.FindBy(actorId).ToString(true, false, true)}");
            }
        }

        private void PrintCastOrCrew(CastOrCrew castOrCrew)
        {
            Console.WriteLine($"{castOrCrew.Name}");
            Console.WriteLine($"Born {castOrCrew.DateOfBirth:yyyy-MM-dd}");
            Console.WriteLine($"{castOrCrew.JobTitle}");

            if (castOrCrew.IsActor)
            {
                Console.WriteLine();
                Console.WriteLine("Actor in the following movies:");
                Console.WriteLine("------------------------------");
                foreach (var movieId in castOrCrew.ActedMovieIds)
                {
                    Console.WriteLine($"{movieService.FindBy(movieId).ToString(true)}");

                    List<Guid> coactorIds = movieService.FindBy(movieId).ActorIds.Where(c => c != castOrCrew.Id).ToList();

                    Console.WriteLine("Coactors:");
                    if (coactorIds.Count == 0)
                    {
                        Console.WriteLine("-");
                    }
                    foreach (Guid coactorId in coactorIds)
                    {
                        List<Guid> coacterActedMovieIds = castOrCrewService.FindBy(coactorId).ActedMovieIds.ToList();

                        var commonMovies = coacterActedMovieIds.Join(castOrCrew.ActedMovieIds,
                                                                        mId1 => mId1,
                                                                        mId2 => mId2,
                                                                        (mId1, mId2) => new { movieId1 = mId1, movieId2 = mId2 })
                                                                        .Where(m => m.movieId1 == m.movieId2);

                        Console.Write(castOrCrewService.FindBy(coactorId).Name);

                        foreach (var item in commonMovies)
                        {
                            Console.Write($", {movieService.FindBy(item.movieId1).Title}");
                        }
                        Console.WriteLine();
                    }
                    
                    Console.WriteLine();
                }
            }

            if (castOrCrew.IsDirector)
            {
                Console.WriteLine();
                Console.WriteLine("Director for the following movies:");
                Console.WriteLine("----------------------------------");
                foreach (var movieId in castOrCrew.DirectedMovieIds)
                {
                    Console.WriteLine($"{movieService.FindBy(movieId).ToString(true)}");
                }
            }
        }

        private void PrintAllMovies()
        {
            var movies = movieService.GetAllMovies();
            foreach (var movie in movies)
            {
                Console.WriteLine(movie.ToString(hideGuids));
            }
        }

        private void PrintAllMoviesDetailed()
        {
            List<Movie> movies = movieService.GetAllMovies() as List<Movie>;
            foreach (Movie movie in movies)
            {
                Console.WriteLine(movie.ToString(hideGuids));
                Console.WriteLine("Actors:");
                List<Guid> actorIds = movie.ActorIds.ToList();
                if (actorIds.Count != 0)
                {
                    foreach (Guid actorId in actorIds)
                    {
                        CastOrCrew actor = castOrCrewService.FindBy(actorId);
                        Console.WriteLine($"* {actor.ToString(true, false, true)}");

                        Console.WriteLine("\tActed in movies:");
                        List<Guid> actedMovieIds = actor.ActedMovieIds.ToList();
                        if (actedMovieIds.Count != 0)
                        {
                            foreach (Guid actedMovieId in actedMovieIds)
                            {
                                Console.Write("\t* ");
                                Console.WriteLine(movieService.FindBy(actedMovieId).ToString(true));
                            }
                        }
                        else
                        {
                            Console.WriteLine("\t-");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\t-");
                }
                Console.WriteLine("----------------------------------");
            }

            Console.WriteLine($"Number of movies: {movies.Count}");
        }

        private void PrintAllCastOrCrew()
        {
            foreach (CastOrCrew castOrCrew in castOrCrewService.GetAllPeople())
            {
                Console.WriteLine(castOrCrew.ToString(hideGuids));
            }
        }

        private void PrintAllCastOrCrewDetailed()
        {
            List<CastOrCrew> people = castOrCrewService.GetAllPeople() as List<CastOrCrew>;
            foreach (CastOrCrew castOrCrew in people)
            {
                Console.WriteLine(castOrCrew.ToString(hideGuids));
                Console.WriteLine("Acted in movies:");

                if (castOrCrew.IsActor)
                {
                    List<Guid> actedMovieIds = castOrCrew.ActedMovieIds.ToList();
                    foreach (Guid movieId in actedMovieIds)
                    {
                        Movie movie = movieService.FindBy(movieId);
                        if (movie == null)
                        {
                            Console.WriteLine("null");
                            continue;
                        }
                        Console.WriteLine(movie.ToString(true));
                    }
                }
                else
                {
                    Console.WriteLine("\t-");
                }

                Console.WriteLine("Directed movies: ");

                if (castOrCrew.IsDirector)
                {
                    List<Guid> directedMovieIds = castOrCrew.DirectedMovieIds.ToList();
                    foreach (Guid movieId in directedMovieIds)
                    {
                        Movie movie = movieService.FindBy(movieId);
                        if (movie == null)
                        {
                            Console.WriteLine("null");
                            continue;
                        }
                        Console.WriteLine(movie.ToString(true));
                    }
                }
                else
                {
                    Console.WriteLine("\t-");
                }
                Console.WriteLine("--------------------");
            }

            Console.WriteLine($"Total cast/crew: {people.Count}");
            Console.WriteLine($"Actors: {castOrCrewService.NumberOfActors()}");
            Console.WriteLine($"Directors: {castOrCrewService.NumberOfDirectors()}");
            Console.WriteLine($"Multitaskers: {castOrCrewService.NumberOfMultitaskers()}");
            Console.WriteLine($"Unemployed: {castOrCrewService.NumberOfUnemployees()}");
        }

        private void AskFor(out string result, string askingPfrase)
        {
            Console.Write(askingPfrase);
            result = Console.ReadLine();
        }

        private void AskFor(out DateTime result, string askingPfrase)
        {
            Console.Write(askingPfrase);
            var line = Console.ReadLine();
            while (!DateTime.TryParse(line, out result))
            {
                Console.Write("Please enter a valid date: ");
                line = Console.ReadLine();
            }
        }

        private void AskFor(out ProductionYear result, string askingPfrase)
        {
            Console.Write(askingPfrase);
            var line = Console.ReadLine();
            while (!ProductionYear.TryParse(line, out result))
            {
                Console.Write("Please enter a valid production year: ");
                line = Console.ReadLine();
            }
        }

        private bool AskForConformationTo(string question, bool hideKey)
        {
            while (true)
            {
                Console.Write($"{question} (Y/n): ");
                ConsoleKey keyInput = Console.ReadKey(hideKey).Key;
                Console.WriteLine();
                if (keyInput == ConsoleKey.Y || keyInput == ConsoleKey.Enter)
                {
                    return true;
                }
                if (keyInput == ConsoleKey.N)
                {
                    return false;
                }
            }
        }
    }
}

